package com.groupama.tdd.test.model;

import static org.junit.Assert.assertEquals;

import java.util.HashSet;
import java.util.Set;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.groupama.tdd.stationnement.enumeration.EtatVoiture;
import com.groupama.tdd.stationnement.enumeration.Prestige;
import com.groupama.tdd.stationnement.model.Berline;
import com.groupama.tdd.stationnement.model.Citadine;
import com.groupama.tdd.stationnement.model.Parking;
import com.groupama.tdd.stationnement.model.Voiture;

public class ParkingTest {
	
	private Parking parking;
	
	private Set<Voiture> voitures;
	
	private long nombreCitadines;
	
	private long nombreBerlines;
	
	@Before
	public void setUp() {
		this.parking = new Parking(5L);
		this.voitures = new HashSet<>();
		
		this.voitures.add(new Citadine(1L, "Peugeot 108", (float) 1.75, EtatVoiture.STATIONNEE, 4, "Essence"));
		this.voitures.add(new Citadine(2L, "Dacia Sandero", (float) 2.25, EtatVoiture.STATIONNEE, 4, "Essence"));
		this.voitures.add(new Berline(3L, "Renault Megane", (float) 2.40, EtatVoiture.STATIONNEE, 4, Prestige.MOYENNE_GAMME));
		this.voitures.add(new Berline(4L, "Opel Astra", (float) 2.60, EtatVoiture.STATIONNEE, 4, Prestige.MOYENNE_GAMME));
		this.voitures.add(new Berline(5L, "Audi A4", (float) 3, EtatVoiture.STATIONNEE, 4, Prestige.HAUT_DE_GAMME));
		
		this.parking.setVehicules(this.voitures);
		
		this.nombreCitadines = 2;
		this.nombreBerlines = 3;
	}
	
	@After
	public void tearDown() {
		this.parking = null;
		this.voitures = null;
	}
	
	@Test
	public void doitRetournerDeux_lorsqueObtenirNombreDeCitadines() {
		long resultat = this.parking.obtenirNombreDeCitadines();
		
		assertEquals(this.nombreCitadines, resultat);
	}
	
	@Test
	public void doitRetournerZero_lorsqueObtenirNombreDeCitadinesEtPasDeVehicules() {
		this.parking.setVehicules(new HashSet<Voiture>());
		
		long resultat = this.parking.obtenirNombreDeCitadines();
		
		assertEquals(0, resultat);
	}
	
	@Test
	public void doitRetournerTrois_lorsqueObtenirNombreDeBerlines() {
		long resultat = this.parking.obtenirNombreDeBerlines();
		
		assertEquals(this.nombreBerlines, resultat);
	}
	
	@Test
	public void doitRetournerZero_lorsqueObtenirNombreDeBerlinesEtPasDeVehicules() {
		this.parking.setVehicules(new HashSet<Voiture>());
		
		long resultat = this.parking.obtenirNombreDeBerlines();
		
		assertEquals(0, resultat);
	}
	
}
