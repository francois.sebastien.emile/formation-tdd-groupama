package com.groupama.tdd.stationnement.model;

import java.util.HashSet;
import java.util.Set;

import com.groupama.tdd.stationnement.enumeration.EtatParking;

import lombok.Getter;
import lombok.Setter;

public class Parking {

	@Getter
	@Setter
	private Set<Voiture> vehicules;
	
	@Getter
	@Setter
	private long nombreDePlacesMaximum;
	
	@Getter
	@Setter
	private EtatParking etat = EtatParking.FERME;
	
	public Parking(long nombreDePlaces) {
		this.vehicules = new HashSet<>();
		this.nombreDePlacesMaximum = nombreDePlaces;
	}
	
	public void ajouterVoiture(Voiture voiture) {
		// TODO
	}
	
	public void retirerVoiture(Voiture voiture) {
		// TODO
	}
	
	public void ouvrir() {
		// TODO
	}
	
	public void fermer() {
		// TODO
	}
	
	public long obtenirNombreDeCitadines() {
		return vehicules.stream().filter(v -> v instanceof Citadine).count();
	}
	
	public long obtenirNombreDeBerlines() {
		return vehicules.stream().filter(v -> v instanceof Berline).count();
	}
	
	
}
