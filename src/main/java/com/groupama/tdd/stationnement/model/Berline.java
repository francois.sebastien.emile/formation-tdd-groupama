package com.groupama.tdd.stationnement.model;

import com.groupama.tdd.stationnement.enumeration.EtatVoiture;
import com.groupama.tdd.stationnement.enumeration.Prestige;

import lombok.Getter;
import lombok.Setter;

public class Berline extends Voiture {

	@Getter
	@Setter
	protected Prestige prestige;
	
	public Berline(Long id, String nom, float longueur, EtatVoiture etat, int nombrePlaces, Prestige prestige) {
		super(id, nom, longueur, etat, nombrePlaces);
		this.prestige = prestige;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((prestige == null) ? 0 : prestige.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Berline other = (Berline) obj;
		if (prestige != other.prestige)
			return false;
		return true;
	}
	
}
