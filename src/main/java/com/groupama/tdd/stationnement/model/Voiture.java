package com.groupama.tdd.stationnement.model;

import com.groupama.tdd.stationnement.enumeration.EtatVoiture;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
public abstract class Voiture {
	
	@Getter
	@Setter
	protected Long id;
	
	@Getter
	@Setter
	protected String nom;
	
	@Getter
	@Setter
	protected float longueur;
	
	@Getter
	@Setter
	protected EtatVoiture etat;
	
	@Getter
	@Setter
	protected int nombrePlaces;

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((etat == null) ? 0 : etat.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + Float.floatToIntBits(longueur);
		result = prime * result + ((nom == null) ? 0 : nom.hashCode());
		result = prime * result + nombrePlaces;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Voiture other = (Voiture) obj;
		if (etat != other.etat)
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (Float.floatToIntBits(longueur) != Float.floatToIntBits(other.longueur))
			return false;
		if (nom == null) {
			if (other.nom != null)
				return false;
		} else if (!nom.equals(other.nom))
			return false;
		if (nombrePlaces != other.nombrePlaces)
			return false;
		return true;
	}
	
}
