package com.groupama.tdd.stationnement.model;

import com.groupama.tdd.stationnement.enumeration.EtatVoiture;

import lombok.Getter;
import lombok.Setter;

public class Citadine extends Voiture {
	
	@Getter
	@Setter
	protected String motorisation;
	
	public Citadine(Long id, String nom, float longueur, EtatVoiture etat, int nombrePlaces, String motorisation) {
		super(id, nom, longueur, etat, nombrePlaces);
		this.motorisation = motorisation;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((motorisation == null) ? 0 : motorisation.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Citadine other = (Citadine) obj;
		if (motorisation == null) {
			if (other.motorisation != null)
				return false;
		} else if (!motorisation.equals(other.motorisation))
			return false;
		return true;
	}
	
}
