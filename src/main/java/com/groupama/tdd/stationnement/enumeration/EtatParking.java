package com.groupama.tdd.stationnement.enumeration;

public enum EtatParking {
	
	OUVERT(1), FERME(2);

	private int value;
	
	private EtatParking(int value) {
		this.value = value;
	}
	
	public int getValue() {
		return this.value;
	}
	
}
