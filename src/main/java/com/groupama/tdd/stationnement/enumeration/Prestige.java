package com.groupama.tdd.stationnement.enumeration;

public enum Prestige {

	BAS_DE_GAMME(1), MOYENNE_GAMME(2), HAUT_DE_GAMME(3), LUXE(4);
	
	private int value;
	
	private Prestige(int value) {
		this.value = value;
	}
	
	public int getValue() {
		return this.value;
	}
}
