package com.groupama.tdd.stationnement.enumeration;

public enum EtatVoiture {
	
	STATIONNEE(1), NON_STATIONNEE(2), EN_COURS_DE_STATIONNEMENT(3);
	
	private int value;
	
	private EtatVoiture(int value) {
		this.value = value;
	}
	
	public int getValue() {
		return this.value;
	}
	
}
