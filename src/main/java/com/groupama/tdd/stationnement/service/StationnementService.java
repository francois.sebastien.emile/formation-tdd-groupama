package com.groupama.tdd.stationnement.service;

import com.groupama.tdd.stationnement.model.Parking;
import com.groupama.tdd.stationnement.model.Voiture;

public interface StationnementService {
	
	void garerVoiture(Voiture voiture, Parking parking);
	
	void recupererVoiture(Voiture voiture, Parking parking);
	
}
