package com.groupama.tdd.stationnement.service;

import com.groupama.tdd.stationnement.model.Parking;

public interface ParkingService {
	
	int consulterPlacesLibres(Parking parking);
	
	void ouvrirParking(Parking parking);
	
	void fermerParking(Parking parking);
	
}
